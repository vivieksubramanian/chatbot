package com.pizza.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtil {

	public static Long getResultAsLong(String sql) throws Exception
	{
		Long val = 0l;
		Connection con = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizza", "root", "root");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if(rs != null && rs.next()) {
				val = rs.getLong(1);
				
			}
		}
		finally
		{
			if(con != null)
				con.close();
		}
		return val;
	}
	public static List<List<String>> getResultAsList(String sql, int noOfResults) throws Exception
	{
		List<List<String>> list = new ArrayList<>();
		Connection con = null;
		try
		{
			
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizza", "root", "root");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if(rs == null)
				return list;
			while(rs.next())
			{
				
				List<String> list1 = new ArrayList<>();
				for(int i=0;i<noOfResults;i++)
				{
					
					list1.add(rs.getString(i+1));
				}
				list.add(list1);
			}
		}
		finally
		{
			if(con != null)
				con.close();
		}
		return list;
	}
	/*public static Map<String, String> getResultAsMap(String sql) throws Exception
	{
		Map<String, String> map = new HashMap<String, String>();
		Connection con = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizza", "root", "root");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if(rs != null)
			{
				String key = rs.getString(1);
				String value = rs.getString(2);
				map.put(key, value);
			}
			
		}
		finally
		{
			if(con != null)
				con.close();
		}
		return map;
	}*/
	
	public static int executeUpdate(String sql) throws Exception
	{
		Connection con = null;
		int i=0;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizza", "root", "root");
			Statement stmt = con.createStatement();
			i = stmt.executeUpdate(sql);
		}
		finally
		{
			con.close();
		}
		return i;
	}
	
	public static boolean hasResult(String sql) throws Exception
	{
		Connection con = null;
		boolean b = false;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pizza", "root", "root");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			if(rs!=null)
				b = rs.next();
		}
		finally
		{
			if(con != null)
				con.close();
		}
		return b;
	}
	
	

}
