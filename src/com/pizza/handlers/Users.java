package com.pizza.handlers;

import java.util.List;

import com.pizza.dao.DBUtil;

public class Users {
	
	public static long validate(String email,String pass) throws Exception
	{
		String sql = "select uid from UserDetails where email='"+email+"' and password='"+pass+"'";
		return DBUtil.getResultAsLong(sql);
	}
	
	public static int registerUser(String name,String email,String mob_no,String address,int pincode,String password) throws Exception
	{
		System.out.println("\n\nemail: "+email+" pass: "+password+"\n\n");
		String sql1 = "select uid from UserDetails where email = '"+email+"'";
		if(!DBUtil.hasResult(sql1))
		{
			String sql = "insert into UserDetails(name, email, phone_number, address, pincode, password) values ('"+name+"', '"+email+"' , '"+mob_no+"', '"+address+"', "+pincode+", '"+password+"')";
	
			DBUtil.executeUpdate(sql);
			return 1;
		}
		else
		{
			return 2;
		}
		
	}
	
	public static List<List<String>> fetchAllPizza() throws Exception
	{
		String sql = "select  * from Stock";
		List<List<String>> list = DBUtil.getResultAsList(sql, 4);
		return list;
	}
	public static List<List<String>> fetchAllStock(int sid) throws Exception {
		String sql="select * from Stock where item_id="+sid;
		List<List<String>> list = DBUtil.getResultAsList(sql, 4);
		return list;
	}
	
	public static int placeOrder(int uid,String items,String price_p_q,String quan,String total,String item_id) throws Exception {
		String quant[]=quan.split(",");
		String item[]=item_id.split(",");
		for(int i=0;i<item.length;i++) {
			int n=Integer.parseInt(item[i]);
			int m=Integer.parseInt(quant[i]);
			long total_quantity = DBUtil.getResultAsLong("select quantity_available from Stock where item_id="+n);
			System.out.println(total_quantity);
			if(((int)total_quantity-m) < 0) {
				return 2;
			}
		}
	
		String sql = "insert into Orders(uid,items_purchased,price_per_quantity,quantity_purchased,total_price) values("+uid+",'"+items+"','"+price_p_q+"','"+quan+"','"+total+"')";
		
		for(int i=0;i<item.length;i++) {
			int n=Integer.parseInt(item[i]);
			int m=Integer.parseInt(quant[i]);
			String sql1 = "update Stock set quantity_available=quantity_available-"+m+" where item_id="+n;
			int value=DBUtil.executeUpdate(sql1);
			System.out.println(value+" updated stock");
		}
		int value = DBUtil.executeUpdate(sql);
		System.out.println(value+" inserted status 1->success or ->fail");
		return value;
	}
	public static List<List<String>> fetchAllOrders(int uid)throws Exception {
		String sql="select * from Orders where uid="+uid+" order by oid desc";
		List<List<String>> list = DBUtil.getResultAsList(sql, 7);
		return list;
		
	}
	public static List<List<String>> fetchOrderStatus(int uid) throws Exception {
		String sql="select items_purchased,order_status from Orders where uid="+uid+" order by oid desc limit 1";
		String sql1 = "select name from UserDetails where uid="+uid;
		List<List<String>> list = DBUtil.getResultAsList(sql, 2);
		list.add(DBUtil.getResultAsList(sql1, 1).get(0));
		return list;
	}
	public static List<List<String>> fetchUser(int uid) throws Exception {
		String sql="select name from UserDetails where uid="+uid;
		List<List<String>> list=DBUtil.getResultAsList(sql, 1);
		return list;
	}
	
	
	
}