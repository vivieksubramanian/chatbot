package com.pizza.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.pizza.handlers.Users;

/**
 * Servlet implementation class AdminServlet
 */
//@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String USER_RESGITERED = "1";
	private static final String USER_ALREADY_EXIST = "2";
	private static final String WRONG_USER = "3";

	private static final String SUCCESS = "4";
	private static final String ERROR = "5";
	private static final String ORDERPLACED = "6";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String methodName = (String) request.getParameter("method-name");
		System.out.println("methodName: "+methodName);
		switch(methodName)
		{
		case "registerUser":
		{
			try {
				String email = request.getParameter("email");
				String name = request.getParameter("name");
				String mob_no = request.getParameter("mob_no");
				String address = request.getParameter("address");
				int pincode = Integer.parseInt(request.getParameter("pincode"));
				String password = request.getParameter("password");
				
				int result = Users.registerUser(name,email,mob_no,address,pincode,password);
				if(result == 1)
				{
					response.addHeader("status", USER_RESGITERED);
					System.out.println("User Registered");
				}
				else if(result == 2)
				{
					response.addHeader("status", USER_ALREADY_EXIST);
					System.out.println("already registered");
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("/views/login.jsp") ;
				dispatcher.forward(request, response);
				response.addHeader("status", SUCCESS);
				
			}
			catch(Exception e) {
				response.addHeader("status", ERROR);
				e.printStackTrace();
			}
			break;
		}
		case "login":
		{
			try
			{
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				System.out.println("\n\nLogin  email: "+email+" pass: "+password+"\n\n");
				long uid = Users.validate(email,password);
				System.out.println(uid);
				if(uid != 0)
				{
					Cookie ck = new Cookie("PIZZA_USER", ""+uid);
					response.addCookie(ck);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/views/first.jsp") ;
					dispatcher.forward(request, response);
				}
				else
				{
					response.addHeader("status", WRONG_USER);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/views/login.jsp") ;
					dispatcher.forward(request, response);
				}

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;
		}
		case "logout":
		{
			try {
				Cookie[] ck = request.getCookies();
				if(ck.length != 0)
				{

					for(Cookie cookie : ck)
					{
						if(cookie.getName().equals("PIZZA_USER"))
						{
							cookie.setValue("");
				            cookie.setPath("/");
							cookie.setMaxAge(0);
							
						}
					}
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("/views/login.jsp") ;
				dispatcher.forward(request, response);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			break;
		}
		case "chat":
		{
			String message = request.getParameter("umsg"); 
			System.out.println(message+" >got<");
			switch(message)
			{
			case "showMenu":
			{
				try
				{
					System.out.println("showing");
					List<List<String>> list = Users.fetchAllPizza();
					JSONObject json = new JSONObject();
					json.put("rows", list);
					response.getWriter().write(json.toString());
					response.addHeader("status", SUCCESS);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				break;
			}
			}
			break;
		}
		case "Bill":
		{
			int uid=Integer.parseInt(request.getParameter("umsg"));
			try
			{
				System.out.println("showingBill");
				List<List<String>> list = Users.fetchAllStock(uid);
				JSONObject json = new JSONObject();
				json.put("rows", list);
				response.getWriter().write(json.toString());
				response.addHeader("status", SUCCESS);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		case "order":
		{
			try {
			int uid = Integer.parseInt(request.getParameter("userid"));
			String items = request.getParameter("items");
			String quantities = request.getParameter("quantities");
			String price_p_q = request.getParameter("price_p_q");
			String total = request.getParameter("total");
			String item_id = request.getParameter("item_id");
			int result = Users.placeOrder(uid,items,price_p_q,quantities,total,item_id);
			System.out.println(uid+" order");
			if(result == 1) {
				response.addHeader("status", ORDERPLACED);
	
			}
			else {
				System.out.println("Order not placed");
			}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			break;
		}
		case "orderHistory":
		{
			int uid=Integer.parseInt(request.getParameter("umsg"));
			try
			{
				System.out.println("showingOrderHistory");
				List<List<String>> list = Users.fetchAllOrders(uid);
				JSONObject json = new JSONObject();
				json.put("rows", list);
				response.getWriter().write(json.toString());
				response.addHeader("status", SUCCESS);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		case "orderStatus":
		{
			int uid=Integer.parseInt(request.getParameter("umsg"));
			try
			{
				System.out.println("orderStatus");
				List<List<String>> list = Users.fetchOrderStatus(uid);
				JSONObject json = new JSONObject();
				json.put("rows", list);
				response.getWriter().write(json.toString());
				response.addHeader("status", SUCCESS);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		case "getName":
		{
			int uid=Integer.parseInt(request.getParameter("id"));
			try
			{
				System.out.println("name");
				List<List<String>> list = Users.fetchUser(uid);
				JSONObject json = new JSONObject();
				json.put("rows", list);
				response.getWriter().write(json.toString());
				response.addHeader("status", SUCCESS);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
