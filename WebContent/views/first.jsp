<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PizzaShop</title>

<link rel="stylesheet" href="style/first_page_style.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="navbar">
            <div class="links">
             <ul>
                 <a href="#"><li>HOME</li></a>
                 <a href="#"><li>VARIETIES</li></a>
                </ul>
            </div>
            <form action="AdminServlet" method="post">
            <input type="hidden" name="method-name" value="logout">
            <button type="submit" class="btn">LOGOUT</button> 
            </form>
            
        
                         
           <div class="banner">
            
               <h1>Food Doesn't<span> have <br>a Religion.</span>It is a Religion</h1>
               <button class="ex" type="button">Explore</button>
            </div>

                <div class="vertical">
                    <div class="search">
                     <i class="fa fa-search" ></i>
            </div>
       
                    <div class="icons">
                        <i class="fa fa-facebook-official" ></i>
                        <i class="fa fa-twitter" ></i>
                        <i class="fa fa-instagram" ></i>
                                
                    </div>
            </div>
        </div>
        <div id="bot">
        	<input type="image" onclick="chat()" src="images/bot1.png" id="botimage" alt="bot" height=100>
        </div>
        <iframe src="views/chatBot.jsp" title="chatbot" id="chat" class="ChatBot"></iframe>
        
  
<script type="text/javascript"> 
		
        function preventBack() { 
            window.history.forward();  
        } 
          
        setTimeout("preventBack()", 0); 
          
        window.onunload = function () { null }; 
        
        var temp=0;
        
        function chat() {
        	if(temp==0) {
	        	document.getElementById("chat").style.display="block";
	        	document.getElementById("botimage").src="images/exit.png";
	        	document.getElementById("botimage").style.height="70px";
	        	
	        	temp=1;
        	}
        	else {
        		document.getElementById("chat").style.display="none";
        		document.getElementById("botimage").src="images/bot1.png";
	        	document.getElementById("botimage").style.height="100px";

        		temp=0;
        	}
        }
        
    </script> 
</body>
</html>