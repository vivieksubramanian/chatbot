<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PizzaShop</title>
<link rel="stylesheet" type="text/css" href="style/login_style.css">
<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
<script type="text/javascript"> 
        function preventBack() { 
            window.history.forward();  
        } 
          
        setTimeout("preventBack()", 0); 
          
        window.onunload = function () { null }; 
    </script> 
</head>
<body>
<div class="login">
                <h1>Login Here</h1>
            <form action="AdminServlet" method="post">
            <input type="hidden" name="method-name" value="login">
                <p>Email</p>
            <input  type="text" name="email" required>
                <p>Password</p>
            <input  type="password" name="password" required>
            <input type="submit" class="btn" value='Login'><br>
               
              <br>  <a href="views/signup.jsp">Don't have an Account?</a>
            </form>
        </div>

</body>

</html>