<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PizzaShop</title>
<link rel="stylesheet" type="text/css" href="../style/login_style.css">
<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
</head>
<body>
<div class="login">
                <h1>signup Here</h1>
            <form action="../AdminServlet" method="post">
            <input  type="hidden" name="method-name" value="registerUser">
                <p>User name</p>
            <input  type="text" name="name" required>
            	<p>Email</p>
            <input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Enter valid email_id" size=24 name="email" required> 
            	<p>Mobile_number</p>
            <input type="text" name="mob_no" size=10 required>
            	<p>Address</p>
            <input type="text" name="address" required>
            	<p>Pincode</p>
            <input type="text" name="pincode" required> 
                <p>Password</p>
            <input  type="password" name="password" pattern="(?=.*\d)(?=.*[a-z]).{8,}" title="Password must contain atleast one lowercase and atleast 8 or more characters" required>
            <input type="submit" class="btn" value='signup'><br><br>
            <input type="reset" class="btn" value="clear"><br> 
            </form>
        </div>
</body>
</html>