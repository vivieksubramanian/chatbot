<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Bot</title>
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../style/chat_bot.css">
<script src="../js/functions.js"></script>
<script src="../js/jquery.js"></script>
</head>
<body>
	<div id="heading" class="container">
		<div>
			<img src="../images/bot1.png" alt="bot"> <i><b>PIZZA
					BOY <br> Assistant
			</b></i><br>
		</div>
		
	</div>
	<div id="chatBox"></div>
		<div id="form">
			<input id="typed" type="text" placeholder="type a message" size=25
				name="umsg">
			<button type="submit" class="btn btn-grp" onclick="usermsg()"
				style="background-color: yellow;">send</button>

		</div>
<script type="text/javascript">
	getUID();
	start();
	function start() {
		var url = "/PizzaShop/AdminServlet?method-name=getName&id="+userId;
		var user = sendHttpRequest(url);
		var json=JSON.parse(user);
		console.log(status);
		var rows=json.rows;
		var name=rows[0]
		ans='<div><p>Hi '+name[0]+' how can I help you?</p></div>';
		$("#chatBox").append(ans);
		initial();
	}
	function getUID()
	{
		cookie = document.cookie.split("=");
		userId=cookie[1];
		console.log("cookies: "+cookie[0]+" "+cookie[1]);
	}
	
	function initial() {
		$("#chatBox").append('<div id="startdiv"><input class="btn-sm btn-primary" type="button" onclick="menu()" value="showMenu"> <input type="button" class="btn-sm btn-primary" onclick="orderStatus()" value="order status"> <input class="btn-sm btn-primary" type="button" onclick="orderHistory()" value="my order history"></div>');
		scroll();
	}
	function menu() {
		$("#chatBox").append('<div><p>You are Just 2 steps away to order your <b>PIZZA</b><br><p>1.Click on ShowMenu<br>2.Enter selected item_number which is shown in menu and quantity in chat box in given pattern<br><span style="color:red">For single item -> 10-2 (item_number-quantity)<br> For more items -> (item_number-quantity/item_number-quantity/...) </span></B></p></p></div>');
		var title="showMenu";
		var url = "/PizzaShop/AdminServlet?method-name=chat&umsg="+title;
		var cdetails = sendHttpRequest(url);
		json = JSON.parse(cdetails);
		rows=json.rows;
		$("#chatBox").append('<div><table id="menu" class="table table-condensed table-hover"><thead><tr><th>ID</th><th>Item</th><th>Price</th><th>Quantity_available</th></tr></thead><tbody></tbody></table></div><br>');
		for(var i=0;i<rows.length;i++) {
			var row=rows[i];
			var id=row[0];
			var pizza_name = row[1];
			var quantity = row[2];
			var pricePerQuantity = row[3];
			$("#menu > tbody:last-child").append('<tr><td>'+id+'</td><td>'+pizza_name+'</td><td>'+pricePerQuantity+'</td><td>'+quantity+'</td></tr>');
		}
		$("#chatBox").append('<div><B>Hey buddy, Enter selected item_number with quantity in chat box <span style="color:red">for example: 10-2/6-1 </span></B></div>');
		scroll();
	}
	function orderStatus() {
		var url = "/PizzaShop/AdminServlet?method-name=orderStatus&umsg="+userId;
		var status = sendHttpRequest(url);
		var json=JSON.parse(status);
		console.log(status);
		var rows=json.rows;
		var name1=rows[1]
		$("#chatBox").append("<div><p>Hello "+name1[0]+" here is your recent order status...\'_\'</p></div>");
		$("#chatBox").append('<div id="his" style="overflow-x:scroll;"><table id="status" class="table table-condensed table-hover"><thead><tr><th>Items</th><th>status</th></tr></thead><tbody></tbody></table></div><br>');
		
		var row=rows[0];
		$("#status > tbody:last-child").append('<tr><td>'+row[0]+'</td><td>'+row[1]+'</td></tr>');
		initial();
		scroll();
	
	}
	function orderHistory() {
		var url = "/PizzaShop/AdminServlet?method-name=orderHistory&umsg="+userId;
		var history = sendHttpRequest(url);
		var json=JSON.parse(history);
		var rows=json.rows;
		$("#chatBox").append('<div id="his" style="overflow-x:scroll;"><table id="History" class="table table-condensed table-hover"><thead><tr><th>oid</th><th>Items</th><th>Item_price</th><th>Item_quantity</th><th>amount</th><th>status</th></tr></thead><tbody></tbody></table></div><br>');
		for(var j=0;j<rows.length;j++) {
			var row=rows[j];
			$("#History > tbody:last-child").append('<tr><td>'+row[0]+'</td><td>'+row[2]+'</td><td>'+row[3]+'</td><td>'+row[4]+'</td><td>'+row[5]+'</td><td>'+row[6]+'</td></tr>');
			
		}
		initial();
		scroll();
		
	}
	
	function usermsg() {
		msg=document.getElementById("typed").value;
		$("#chatBox").append("<div><p style=\"position:relative;right:0px;left:25%;background-color:#aed6f1;\">"+msg+"</p></div></div>");
		$('#typed').val('');
		var temp='0';
		if(msg=="") {
			$("#chatBox").append('<div><p>Your entry is not valid please retry thank you</p></div><br>');
			scroll();
		}
		else if(msg.includes("/") && msg.includes("-")) {
			orders = msg.split("/");
			console.log(orders);
			for(var i=0;i<orders.length;i++) {
				var items = orders[i].split("-");
				console.log(items);
				if(items.length != 2) {
					$("#chatBox").append('<div><p>Your entry is not valid please retry thank you</p></div><br>');
					temp='1';
					break;
				}
			}
			if(temp=='0') {
				place_id="";
				place_item="";
				place_quantity="";
				place_price_per_item="";
				total_bill=0;
				$("#chatBox").append('<div><table id="bill" class="table table-condensed table-hover"><thead><tr><th>Items</th><th>Quantity</th><th>Price_P_Q</th><th>Price</th></tr></thead><tbody></tbody></table></div>');
				
				console.log("order-length "+orders.length);
				for(var i=0;i<orders.length;i++) {
					console.log(i+" loop");
					var items = orders[i].split("-");
					var url = "/PizzaShop/AdminServlet?method-name=Bill&umsg="+items[0];
					var cdetails = sendHttpRequest(url);
					json = JSON.parse(cdetails);
					console.log(json);
					rows=json.rows;
					var row = rows[0];
					var id=row[0];
					var pizza_name = row[1];
					var quantity = row[2];
					var pricePerQuantity = row[3];
					var total = pricePerQuantity*items[1];
					$("#bill > tbody:last-child").append('<tr><td>'+pizza_name+'</td><td>'+items[1]+'</td><td>'+pricePerQuantity+'</td><td>'+total+'</td></tr>');
					if(i!=orders.length-1) {
						total_bill+=total;
						place_id += id+",";
						place_item+=pizza_name+",";
						place_quantity += items[1]+",";
						place_price_per_item += pricePerQuantity+",";
					}
				}
				if(temp=='0') {
					total_bill+=total;
					place_id += id;
					place_item+=pizza_name;
					place_quantity += items[1];
					place_price_per_item += pricePerQuantity;
					
				$("#bill > tbody:last-child").append('<tr><td>TOTAL</td><td>'+total_bill+'</td></tr>');
	
				$("#chatBox").append("<div><input type=\"button\" onclick=\"placeOrder(\'"+userId+"\',\'"+place_item+"\',\'"+place_quantity+"\',\'"+place_price_per_item+"\',\'"+total_bill+"\',\'"+place_id+"\')\" class=\"btn-sm btn-primary\" value=\"confirm order\"> <input onclick=\"initial()\" type=\"button\" class=\"btn-sm btn-primary\" value=\"cancel\"></div><br>");
				scroll();
				}
			}
		}
		else if(msg.includes("-")) {
			temp1='0';
			var orders = msg.split("-");
			var url = "/PizzaShop/AdminServlet?method-name=Bill&umsg="+orders[0];
			var cdetails = sendHttpRequest(url);
			json = JSON.parse(cdetails);
			rows=json.rows;
			var row=rows[0];
			var id=row[0];
			var pizza_name = row[1];
			var quantity = row[2];
			var pricePerQuantity = row[3];
			var total = pricePerQuantity*orders[1];
			if(temp1 == '0') {
				$("#chatBox").append('<table id="bill" class="table table-condensed table-hover"><thead><tr><th>Items</th><th>Quantity</th><th>Price_p_q</th><th>Price</th></tr></thead><tbody></tbody></table>');
	
				$("#bill > tbody:last-child").append('<tr><td>'+pizza_name+'</td><td>'+orders[1]+'</td><td>'+pricePerQuantity+'</td><td>'+total+'</td></tr>');
				$("#bill > tbody:last-child").append('<tr><td>TOTAL</td><td>'+total+'</td></tr>');
		
				$("#chatBox").append("<div><button type=\"button\" onclick=\"placeOrder(\'"+userId+"\',\'"+pizza_name+"\',\'"+orders[1]+"\',\'"+pricePerQuantity+"\',\'"+total+"\',\'"+id+"\')\" class=\"btn btn-primary\">confirm order</button> <button onclick=\"initial()\" type=\"button\" class=\"btn btn-primary\">cancel</button></div><br>");
				scroll();
			}
		}
		else {
			$("#chatBox").append('<div><p>Your entry is not valid please retry thank you</p></div><br>');
			initial();
			scroll();
	
		}
	}
	
	function placeOrder(uid,items,quantities,price_p_q,total_amount,item_id) {
		console.log(uid+" "+items);
		var url="/PizzaShop/AdminServlet?method-name=order&userid="+uid+"&items="+items+"&quantities="+quantities+"&price_p_q="+price_p_q+"&total="+total_amount+"&item_id="+item_id;
		var orderdetails = sendHttpRequest(url);
		console.log(orderdetails);
		$("#chatBox").append('<div><p>HURRAY you placed your order...</p></div><br>')
		initial();
		scroll();
		
	}
	function scroll() {
	var objDiv = document.getElementById("chatBox");
	objDiv.scrollTop = objDiv.scrollHeight;
	}
	setInterval(scroll, 30000);

</script>
</body>
</html>